
BM25

`_sore = IDF * TF * (n*Boost-value(2.2))`

- `TF` (Term frequency): Đánh giá tần suất xuất hiện của term trong field. Càng xuất hiện nhiều, relevance càng cao\
  Còn nữa, nếu độ dài doc ngán hơn độ dài trung bình trên toàn index thì sẽ có số điểm cao hơn.
  `TF = ((k + 1) * freq) / (k + (1.0 - b + b * L) freq)`\
  + `k`: hằng số (default 1.2)
  + `freq`(`doc.freq`): frequency, số lần term suất hiện trong doc
  + `b`: default 0.75
  + `L`: tỉ lệ giữa độ dài của doc so với độ dài trung bình của tất cả doc `L = fieldLength / avgFieldLength`
  
  ![](https://images.viblo.asia/28dd8beb-fecb-4d36-874d-23f438ed6c19.png)
  + TF score dần tiệm cận đến (k + 1), k càng cao độ hội tụ càng lâu
  + b càng lớn thì độ ảnh hưởng của document length càng lớn
  
- `IDF`(Inverse document frequency): đánh giá tần suất xuất hiện của term trên toàn bộ index. Càng xuất hiện nhiều thì doc càng ít liên quan -> tính đặc biệt\
VD: đang muốn tìm hiểu 'thuật toán bm25 là gì?'
    + từ khóa 'thuật toán' => 10.000 kq.
    +  từ khóa 'BM25' => 5.000 kq --> ít hơn nhưng đúng hơn
    + ==> BM25 > thuật toán
    
    `IDF = log(1 + (docCount - docFreq + 0.5)/(docFreq + 0.5))` 
    + `docCount`: số lượng document trên toàn index
    + `docFreq`: số lượng document chứa term
    
    ![](https://images.viblo.asia/6fe49e50-c6ab-4a5b-9ed8-ff1baf928dc8.png)
    
        
