<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        \App\Document::create([
            'title' => 'Báo Anh ca ngợi chống Covid-19 \'giá rẻ\' kiểu Việt Nam và Ấn Độ',
            'content' => "Báo The Economist nhận định bí quyết chống Covid-19 của Việt Nam cùng bang Kerala, Ấn Độ, là hệ thống y tế công cộng hiệu quả và phản ứng nhanh.
Điện thoại reo, một bác sĩ nhấc máy. \"Thưa ngài, chúng ta hết máy thở rồi. Chúng ta phải làm gì nếu có thêm bệnh nhân đến\", nhân viên y tế nói và thông báo căn bệnh đã giết chết ba trong số bốn bệnh nhân. Không có vaccine hay phác đồ điều trị.
Cuộc nói chuyện giữa các bác sĩ này, dù đã trở nên phổ biến trong Covid-19, nhưng không liên quan đến đại dịch đang hoành hành trên thế giới. Đó là phần mở đầu Virus, bộ phim giành được nhiều lời khen ngợi khi được chiếu tại bang Kerala của Ấn Độ năm ngoái.
Virus phỏng theo câu chuyện có thật về chiến dịch chống đợt bùng phát virus Nipah năm 2018, mầm bệnh từ loài dơi khiến 23 người nhiễm và 21 người chết. Tuy nhiên, Kerala chặn đứng Nipah trong vòng một tháng bằng cách tiếp cận toàn diện gồm phong tỏa toàn bang, liên tục truy vết tiếp xúc và cách ly hàng nghìn người nghi nhiễm."
        ]);


        \App\Document::create([
            'title' => "Trung Quốc nỗ lực viết lại câu chuyện về Covid-19",
            'content' => "Truyền thông Trung Quốc ca ngợi hết lời quyết tâm của chính phủ khi phong tỏa Vũ Hán để ngăn Covid-19, nhưng không đả động gì về nguồn gốc nCoV.
Khi Mỹ và một số đồng minh châu Âu chỉ trích Trung Quốc ngày càng gay gắt về cách ứng phó ban đầu với Covid-19, Bắc Kinh cũng đang đáp trả bằng cách huy động bộ máy truyền thông toàn cầu của mình để kể lại theo cách của họ câu chuyện về đại dịch đã cướp đi sinh mạng của gần 317.000 người trên khắp thế giới.
Trong nỗ lực này, Trung Quốc đang tìm cách truyền đi ba thông điệp chính về đại dịch, theo Rod Wye, phó giáo sư tại Viện nghiên cứu Chatham House ở London và từng là người đứng đầu cơ quan nghiên cứu châu Á tại Bộ Ngoại giao Anh.
Đầu tiên, Bắc Kinh muốn cho thế giới thấy rằng họ đã kiểm soát đại dịch thành công và không ngừng đưa thông tin về các đợt viện trợ vật tư y tế, y bác sĩ tới hỗ trợ các nước khắp thế giới. Hãng thông tấn Xinhua thường xuyên đăng bản tin về những chuyến hàng chở khẩu trang, kit xét nghiệm tới các nước châu Phi hay những đội chuyên gia y tế tới hỗ trợ châu Âu chống dịch, kèm theo đó là những phát biểu biết ơn của đại diện các quốc gia này."
        ]);
    }
}
