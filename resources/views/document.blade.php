@extends('layouts.app')

@section('content')

    <div class="py-12 bg-gray-50 sm:px-6 lg:px-8">
        <h1 class="text-xl">{{$document->title}}</h1>
        <br>
        <div class="desc">
            {!! $document->desc !!}
        </div>
        <br>
        <div class="content">
            {!! $document->content !!}
        </div>
    </div>
    <div class="flex flex-wrap py-12 bg-gray-50 sm:px-6 lg:px-8">
        @foreach($documents_related as $document)
            <div class="px-2 py-3 w-4/12">
                <a href="{{route('document.show', ['id' => $document->id])}}" class="text-xl text-blue-800 block mb-2 break-words">{{$document->title}}</a>
                <div>
                    {{$document->desc}}
                </div>
                @if(!empty($document->hit_score))
                    <p class="text-red-300">score: {{$document->hit_score}}</p>
                @endif
            </div>
        @endforeach
    </div>
@endsection
