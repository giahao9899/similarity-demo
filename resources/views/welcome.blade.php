@extends('layouts.app')

@section('content')
    <div class="flex mt-4">
        <form action="{{route('document.search')}}" method="get" class="ml-auto mr-auto w-full" style="max-width: 700px">
            <input id="doc_search" name="query" class="w-full transition-colors duration-100 ease-in-out focus:outline-0 border border-transparent focus:bg-white focus:border-gray-300 placeholder-gray-600 rounded-lg bg-gray-200 py-2 pr-4 pl-10 block w-full appearance-none leading-normal ds-input" type="text" placeholder="Search the docs (Press &quot;/&quot; to focus)" style="position: relative; vertical-align: top">
        </form>
    </div>

    @if(!empty($keywords))
    <div class="flex mt-4">
        @foreach($keywords as $keyword)
            <a href="{{route('document.search', ['query' => $keyword->name])}}" class="mr-3">#{{$keyword->name}}</a>
        @endforeach
    </div>
    @endif


    @if(!empty($query))
        <h1 class="text-xl">Tìm kiếm cho "{{$query}}"</h1>
    @endif
    <div class="flex flex-wrap min-h-screen py-12 bg-gray-50 sm:px-6 lg:px-8">
        @foreach($documents as $document)
            <div class="px-2 py-3 w-4/12">
                <a href="{{route('document.show', ['id' => $document->id])}}" class="text-xl text-blue-800 block mb-2 break-words">{{$document->title}}</a>
                <div>
                    {{$document->desc}}
                </div>
                @if(!empty($document->hit_score))
                    <p class="text-red-300">score: {{$document->hit_score}}</p>
                @endif
            </div>
        @endforeach
    </div>
    <br>
    <div class="flex flex-wrap text-xl items-stretch text-center text-primary border-t mt-3">
        {{$documents->links()}}
    </div>
@endsection
