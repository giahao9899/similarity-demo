<?php
namespace App\Crawler;

use App\Document;
use App\Keyword;
use Openbuildings\Spiderling\Driver_Phantomjs;
use \Openbuildings\Spiderling\Page;
class VnExpressCrawler
{
    public $baseUrl;

    public function __construct() {
        $this->baseUrl = 'https://vnexpress.net/the-gioi';

    }

    public function crawl() {
        $page = new Page();

        for ($i=1; $i < 10; $i++) {
            if ($i == 1) {
                $url = $this->baseUrl;
            } else {
                $url = $this->baseUrl . "-p$i";
            }
            $page->visit($url);
            $items = $page->all('.item-news.item-news-common');

            foreach ($items as $item) {
                try {
                    $link = $item->find('.title-news a')->attribute("href");
                } catch (\Exception $exception) {
                    break;
                }
                $link = $this->handleUrl($link);
                dump($link);
                $this->crawlPost($link);
            }
        }
    }

    public function crawlPost($url) {
        $page = new Page(new Driver_Phantomjs);
        $page->visit($url);

        try {
            $title = $page->find('.sidebar-1 .title-detail')->text();
            dump($title);
        } catch (\Exception $exception) {
            return;
        }
        $desc = $page->find('.sidebar-1 .description')->text();
        $content = $page->find('.sidebar-1 .fck_detail')->text();

        Document::create([
            'title' => $title,
            'desc' => $desc,
            'content' => $content,
        ]);


    }

    public function handleUrl($url){
        if($position = strpos($url, '?')){
            $url = substr($url, 0, $position);
        }
        return $url;
    }

}