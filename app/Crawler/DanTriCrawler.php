<?php


namespace App\Crawler;


use App\Document;
use App\Keyword;
use Openbuildings\Spiderling\Driver_Phantomjs;
use Openbuildings\Spiderling\Page;

class DanTriCrawler
{
    public $baseUrl;

    public function __construct() {
        $this->baseUrl = 'https://dantri.com.vn/the-gioi';

    }

    public function crawl() {
        $page = new Page();

        for ($i=1; $i < 15; $i++) {
            if ($i == 1) {
                $url = $this->baseUrl . '.htm';
            } else {
                $url = $this->baseUrl . "/trang-$i" . '.htm';
            }
            $page->visit($url);
            $items = $page->all('h2 > a');

            foreach ($items as $item) {
                try {
                    $link = $item->attribute("href");
                } catch (\Exception $exception) {
                    break;
                }
                $link = $this->handleUrl($link);
                $link = 'https://dantri.com.vn' . $link;
                dump($link);
                $this->crawlPost($link);
            }
        }
    }

    public function crawlPost($url) {
        $page = new Page();
        $page->visit($url);

        try {
            $dom_content = $page->find('#ctl00_IDContent_ctl00_divContent');

        } catch (\Exception $ex) {
            dump('Error');
            return;
        }

        $title = $dom_content->find('h1')->text();
        dump($title);
        $desc = $page->find("meta[name='description']")->attribute('content');
        dump($desc);
        $content = $page->find('#divNewsContent')->text();

        $tags = $page->all('.news-tags-item a');

        foreach ($tags as $tag) {
            $tag_name = mb_strtolower($tag->text());
            dump($tag_name);
            $keyword = Keyword::firstOrCreate([
                'name' => $tag_name
            ]);
        }


        Document::create([
            'title' => $title,
            'desc' => $desc,
            'content' => $content,
        ]);


    }

    public function handleUrl($url){
        if($position = strpos($url, '?')){
            $url = substr($url, 0, $position);
        }
        return $url;
    }
}