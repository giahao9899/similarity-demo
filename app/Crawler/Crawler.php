<?php
namespace App\Crawler;

class Crawler
{
    public function __construct() {

    }

    public function crawl() {
        (new VnExpressCrawler())->crawl();
        (new DanTriCrawler())->crawl();
    }
}