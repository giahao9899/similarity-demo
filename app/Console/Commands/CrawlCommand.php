<?php

namespace App\Console\Commands;

use App\Crawler\Crawler;
use App\Document;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CrawlCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:news';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command crawl news';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new Crawler())->crawl();
    }
}
