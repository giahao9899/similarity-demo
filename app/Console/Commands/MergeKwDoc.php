<?php

namespace App\Console\Commands;

use App\Document;
use App\Keyword;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

class MergeKwDoc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'merge:kw_doc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command merge keyword document';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Keyword::chunkById(10, function ($keywords) {
            foreach ($keywords as $keyword) {
                dump($keyword->name);
                $documents = Document::search($keyword->name)->take(200)->get();
                $keyword->documents()->attach($documents);
                $keyword->total_doc = $documents->count();
                dump($keyword->total_doc);
                $keyword->save();
            }
        });
    }
}
