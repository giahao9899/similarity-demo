<?php

namespace App\Console\Commands;

use App\Document;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\DomCrawler\Crawler;

class CrawlWiki extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:wiki {--query=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command crawl wiki';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $query = $this->option('query');
        $param_search = [
            'action' => 'query',
            'format' => 'json',
            'acfrom' => $query,
            'list' => 'allcategories',
            'aclimit' => 30
        ];

        $client = new Client();

        $response = $client->get("https://en.wikipedia.org/w/api.php?" . http_build_query($param_search));

        $res = \GuzzleHttp\json_decode($response->getBody()->getContents());

        foreach ($res->query->allcategories as $cate) {
            $cate = (array)$cate;
            $title = $cate['*'];

            $param_page = [
                'action' => 'parse',
                'page' => $title,
                'prop' => 'text',
                'formatversion' => 2,
                'format' => 'json',
            ];
            dump($title);
            $response = $client->get("https://en.wikipedia.org/w/api.php?" . http_build_query($param_page));

            $res_page = \GuzzleHttp\json_decode($response->getBody()->getContents());
            try {
                $document = new Document();
                $document->title = $res_page->parse->title;
                $document->content = $res_page->parse->text;
                $text = (new Crawler($document->content))->text();
                $desc = substr($text, 0, 512);
                $document->desc = $desc;
                $document->save();
            } catch (\Exception $exception) {
                dump('Error: ');
                Log::error($exception->getMessage());
            }
        }
    }
}
