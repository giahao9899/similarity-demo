<?php

namespace App;

use ScoutElastic\IndexConfigurator;
use ScoutElastic\Migratable;

class SimilarityConfigurator extends IndexConfigurator
{
    use Migratable;

    protected $name = 'similarity_index';

    /**
     * @var array
     */
    protected $settings = [
        "similarity" => [
            "scripted_tfidf" => [
                "type" => "scripted",
                "script" => [
                    "source" => "double tf = Math.sqrt(doc.freq); double idf = Math.log((field.docCount+1.0)/(term.docFreq+1.0)) + 1.0; double norm = 1/Math.sqrt(doc.length); return query.boost * tf * idf * norm;"
                ]
            ],
            'scripted_bm25' => [
                "type" => "BM25",
                'k1' => "1.2",
                'b' => "0.75"
            ],
        ],
        'analysis' => [
            'analyzer' => [
                'vi_std' => [
                    'type' => 'custom',
                    'tokenizer' => 'vi_tokenizer',
                    "filter" => [
                        "lowercase",
                        "stop"
                    ]
                ]
            ]
        ]
    ];
}