<?php

namespace App\Http\Controllers;

use App\Document;
use App\Keyword;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        $documents = Document::paginate(20);

        $keywords = Keyword::orderBy('total_doc', 'desc')->limit(10)->get();

        return view('welcome', compact(
            'documents',
            'keywords'
        ));
    }

    public function show($id) {
        $document = Document::findOrFail($id);

        $documents_related = $documents = Document::search($id)->rule(function ($builder) {
            return [
                'must' => [
                    "more_like_this" => [
                        "fields" => ["title^3", "desc^2", 'content'],
                        "like" => [
                            "_id" => $builder->query
                        ],
                        "min_term_freq" => 1,
                        "max_query_terms" => 12
                    ]
                ]
            ];
        })->get();

        return view('document', compact(
            'document',
            'documents_related'
        ));
    }

    public function search(Request $request) {
        $query = $request->input('query');
        $documents = Document::search($query)->paginate();

        return view('welcome', compact(
            'documents',
            'query'
        ));
    }
}
