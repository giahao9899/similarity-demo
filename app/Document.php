<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class Document extends Model
{
    use Searchable;

    protected $table = 'documents';

    protected $fillable = ['title', 'desc', 'content'];

    public function keyword() {
        return $this->belongsToMany(
            Keyword::class,
            'keyword_document',
            'document_id',
            'keyword_id'
        );
    }

    protected $indexConfigurator = SimilarityConfigurator::class;

    protected $mapping = [
        'properties' => [
            'title' => [
                'type' => 'text',
                'similarity' => 'scripted_bm25',
                "analyzer" => "vi_std",
                "term_vector" => "with_positions_offsets_payloads"
            ],
            'desc' => [
                'type' => 'text',
                'similarity' => 'scripted_bm25',
                "analyzer" => "vi_std",
                "term_vector" => "yes"
            ],
            'content' => [
                'type' => 'text',
                'similarity' => 'scripted_bm25',
                "analyzer" => "vi_std",
                "term_vector" => "yes"
            ]
        ]
    ];
}
