<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'keywords';

    protected $fillable = ['name'];

    public function documents() {
        return $this->belongsToMany(
            Document::class,
            'keyword_document',
            'keyword_id',
            'document_id'
        );
    }
}
